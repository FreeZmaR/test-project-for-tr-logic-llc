<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ TITLE }}</title>
    <script src="<?= "$_SERVER[DOCUMENT_ROOT]/js/index.js" ?>"></script>
    <link rel="stylesheet" href="<?= "$_SERVER[DOCUMENT_ROOT]/css/style.css"?>">
</head>
<body>
{{ TEST }}
</body>
</html>
