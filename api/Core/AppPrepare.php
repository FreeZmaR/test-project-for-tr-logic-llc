<?php

namespace api\Core;

use api\Core\Router\Route\Route;
use api\Core\Router\RouterController\RouterController;


class AppPrepare
{

    protected Route $route;

    public function __construct(RouterController $routes)
    {
        echo "<pre>";
        print_r($_SERVER);
        echo "</pre>";

        switch ($_SERVER['REQUEST_METHOD'])
        {
            case "GET":  $this->setRoute($routes->getRoutesGet());
                break;
            case "POST": $this->setRoute($routes->getRoutesPost());
                break;
            default: break; //TODO: add exception
        }


    }

    public function parseRequestUri(string $uri): string
    {

    }

    private function setRoute(array $routes)
    {
        $patterns = array_keys($routes);

        foreach($patterns as $index => $pattern)
        {
            if (preg_match($pattern, $_SERVER['REQUEST_URI'])) {
               $this->route =  $routes[$pattern];
               break;
            }
        }

        $this->route = new Route();
        $this->route->get404("api");
    }

    public static function parserRoutePattern(string $pattern): string
    {
        return preg_replace('/{id}/', '/[0-9]+/', $pattern);
    }
}