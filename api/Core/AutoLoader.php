<?php


class AutoLoader
{
    public function __construct()
    {
        spl_autoload_register([$this, 'loader']);
    }

    private function loader($className)
    {
        $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
        include_once "$_SERVER[DOCUMENT_ROOT]/../$className.php";
    }
}