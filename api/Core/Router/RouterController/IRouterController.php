<?php


namespace api\Core\Router\RouterController;

use api\Core\Router\Route\IRoute;

interface IRouterController
{
    public function post(string $pattern): IRoute;
    public function get(string $pattern): IRoute;
    public function getRoutesPost(): array;
    public function getRoutesGet(): array;

}