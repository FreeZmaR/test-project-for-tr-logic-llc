<?php

namespace api\Core\Router\RouterController;

use api\Core\AppPrepare;
use api\Core\Router\Route\Route;

class RouterController implements IRouterController
{
    private array $postRouters = [];
    private array $getRouters = [];


    public function post(string $pattern): Route
    {
        $pattern = AppPrepare::parserRoutePattern($pattern);

        $this->postRouters[$pattern] = new Route();

        return $this->postRouters[$pattern];
    }

    public function get(string $pattern): Route
    {
        $pattern = AppPrepare::parserRoutePattern($pattern);

        $this->getRouters[$pattern] = new Route();

        return $this->getRouters[$pattern];
    }

    public function getRoutesPost(): array
    {
        return $this->postRouters;
    }

    public function getRoutesGet(): array
    {
        return $this->getRouters;
    }
}