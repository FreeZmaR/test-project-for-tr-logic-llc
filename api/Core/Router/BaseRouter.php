<?php

namespace api\Core\Router;

use api\Core\Router\RouterController\RouterController;

abstract class BaseRouter
{
    protected RouterController $router;

    public function __construct()
    {
        $this->router = new RouterController();
    }

    abstract public function getEntryPoint(): RouterController;
}