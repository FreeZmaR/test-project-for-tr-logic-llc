<?php


namespace api\Core\Router\Route;


use api\Core\Controller\IController;
use api\Core\Presenter\IPresenter;

abstract class BaseRoute
{
    protected IController $controller;
    protected string $controllerMethod;
    protected IPresenter $presenter;
    protected string $htmlPath;
}