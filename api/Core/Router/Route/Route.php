<?php


namespace api\Core\Router\Route;


use api\Core\Controller\IController;
use api\Core\Presenter\IPresenter;
use api\Core\Utils\HtmlRender;

class Route extends BaseRoute implements IRoute
{
    public function controller(IController $controller, string $method): Route
    {
       $this->controller = $controller;
       $this->controllerMethod = $method;

       return $this;
    }

    public function json(IPresenter $presenter = null): Route
    {
       $this->presenter = $presenter;

       return $this;
    }

    /**
     * argument without extension
     * @param string $htmlTemplate
     * @param array $parameters
     * @return Route
     */
    public function html(string $htmlTemplate, array $parameters = []): Route
    {
       $templatePath = str_replace('.', '/', $htmlTemplate);

       $this->htmlPath = "$_SERVER[DOCUMENT_ROOT]/../Resource/$templatePath.php";

        return $this;
    }

    public function middleware(): Route
    {
        return $this;
    }

    public function get404(string $prefix="")
    {
        if (preg_match("/".$prefix."\//m", $_SERVER['REQUEST_URI'])) {
            die(json_encode([
                "status" => "404",
                "data" => "Page not found",
            ]));
        }

        http_response_code(404);
        HtmlRender::showHtmlPage($_SERVER['DOCUMENT_ROOT'] . "/404.php");
    }
}