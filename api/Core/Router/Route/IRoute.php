<?php

namespace api\Core\Router\Route;

use api\Core\Controller\IController;
use api\Core\Presenter\IPresenter;

interface IRoute
{
    public function controller(IController $controller, string $method): IRoute;
    public function json(IPresenter $presenter = null): IRoute;
    public function html(string $htmlTemplate, array $parameters): IRoute;
    public function middleware(): IRoute;
    public function get404(string $prefix="");
}