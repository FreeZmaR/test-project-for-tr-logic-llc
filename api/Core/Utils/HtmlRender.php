<?php

namespace api\Core\Utils;

class HtmlRender
{

    /**
     * Get html content replace parameters
     * @param string $path
     * @param array $parameters
     * @return string
     */
    public static function getHtmlContent(string $path, array $parameters = []): string {
        return self::replaceParams(file_get_contents($path), $parameters);
    }

    /**
     * Replace parameters and echo html content, die
     * @param string $path
     * @param array $parameters
     */
    public static function showHtmlPage(string $path, array $parameters = []) {
        echo self::getHtmlContent($path, $parameters);
        die();
    }

    /**
     * replace vars in the templates: {{ var.value }}
     * @param string $content
     * @param array $params
     * @param string $prefix
     * @return string
     */
    private static function replaceParams(string $content, array $params, string $prefix = ""): string
    {
        if (! count($params)) {
            return $content;
        }

        foreach ($params as $key => $value) {
            $key = strtoupper($key);

            if (is_array($value)) {
                self::replaceParams($content, $value, $key);
            }

            $key = $prefix ? "$prefix.$key" : $key;
            $content = str_replace("{{ $key }}", $value, $content);
        }

        return $content;
    }
}