<?php

require __DIR__ . "/Core/AutoLoader.php";

use api\Router;

$classLoader = new AutoLoader();

$appRouter = new Router();

new api\Core\AppPrepare($appRouter->getEntryPoint());