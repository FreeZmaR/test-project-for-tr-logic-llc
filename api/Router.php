<?php

namespace api;

use api\Core\Router\BaseRouter;
use api\Core\Router\RouterController\RouterController;

class Router extends BaseRouter
{
    public function getEntryPoint(): RouterController
    {
        $this->router->post("/")->html("index", ['title' => "Main Page", 'test' => 'Some text']);

        return $this->router;
    }
}